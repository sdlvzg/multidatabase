package org.lg.multidatabase.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.lg.multidatabase.bean.User;
import org.lg.multidatabase.mapper.UserMapper;
import org.lg.multidatabase.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public Integer add(String name,Integer age,String email) {
        User user = new User();
        user.setName(name);
        user.setAge(age);
        user.setEmail(email);
        int rs = userMapper.insert(user);
        return rs;
    }

}
