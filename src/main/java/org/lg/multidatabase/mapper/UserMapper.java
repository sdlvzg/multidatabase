package org.lg.multidatabase.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.lg.multidatabase.bean.User;

public interface UserMapper extends BaseMapper<User> {

}