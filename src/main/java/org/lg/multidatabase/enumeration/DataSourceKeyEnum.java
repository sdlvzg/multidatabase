package org.lg.multidatabase.enumeration;
/**
 * @ClassName: DataSourceKeyEnum
 * @Author: lvgang
 * @Date: 2022/4/24 10:03
 * @Description: DataSource Key Enum
 */
public enum DataSourceKeyEnum {

    ONE("ONE", 1,"localhost"),
    TWO("TWO", 2,"127.0.0.1");

    private String name;
    private int index;
    private String domain;

    private DataSourceKeyEnum(String name, int index, String domain) {
        this.name = name;
        this.index = index;
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    public String getDomain() {
        return domain;
    }
}
