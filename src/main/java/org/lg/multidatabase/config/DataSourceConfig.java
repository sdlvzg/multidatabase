package org.lg.multidatabase.config;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @ClassName: DateSourceConfig
 * @Author: lvgang
 * @Date: 2022/4/24 10:03
 * @Description: DataSource配置类
 */
@Configuration
@Slf4j
public class DataSourceConfig {

    /**
     * DataSource1配置
     * @return
     */
    @Bean
    @ConfigurationProperties(prefix = "spring.datasourceone")
    public DataSource dataSourceOne(){
        log.info("=====Init===spring.datasourceone===");
        //创建一个DruidDataSource，根据配置文件
        return DruidDataSourceBuilder.create().build();
    }

    /**
     * DataSource2配置
     * @return
     */
    @Bean
    @ConfigurationProperties(prefix = "spring.datasourcetwo")
    public DataSource dataSourceTwo(){
        log.info("=====Init===spring.datasourcetwo===");
        //创建一个DruidDataSource，根据配置文件
        return DruidDataSourceBuilder.create().build();
    }

}