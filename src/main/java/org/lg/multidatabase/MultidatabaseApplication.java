package org.lg.multidatabase;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("org.lg.multidatabase.mapper")
public class MultidatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultidatabaseApplication.class, args);
	}

}
