package org.lg.multidatabase.controller;

import org.lg.multidatabase.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/add")
    public String add(@RequestParam("name") String name,
                              @RequestParam("age") Integer age,
                              @RequestParam("email") String email) {

        int i = userService.add(name, age,  email);

        if(i == 1){
            return "添加成功";
        }else{
            return "添加失败";
        }
    }

}
